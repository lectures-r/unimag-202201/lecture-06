## Eduard Martinez
## Update: 21-10-2022

## limpiar entonro
rm(list=ls())

## instalar/llamar pacman
require(pacman)

## usar la función p_load de pacman para instalar/llamar las librerías de la clase
p_load(tidyverse, ## manipular/limpiar conjuntos de datos.
       rio, ## para leer/escribir archivos desde diferentes formatos. 
       skimr, ## skim: describir un conjunto de datos
       janitor) ##  tabyl: frecuencias relativas

## **[1.] Combinar conjuntos de datos (adicionar filas/columnas)**

### **1.1 Agregar observaciones**

## Generar conjuntos de datos para hacer la aplicación:
set.seed(0117) # Fijar semilla
obs_1 = tibble(id = 100:105 , 
             age = runif(6,18,25) %>% round() , 
             height = rnorm(6,170,10) %>% round())

obs_2 = tibble(id = 106:107 , 
             age = runif(2,40,50)  %>% round() , 
             height = rnorm(2,165,8) %>% round() , 
             name = c("Lee","Bo"))

## Inspeccionar los datos:
obs_1 %>% head(n = 5)
obs_2 %>% head(n = 2)

## Combinar el conjunto de datos:
data = bind_rows(obs_1,obs_2, .id = "group")
data

### **1.2 Adicionar variables a un conjunto de datos**
db_1 <- tibble(id = 102:105 , income = runif(4,1000,2000) %>% round())
db_2 <- tibble(id = 103:106 , age = runif(4,30,40)  %>% round())

## Inspeccionar los datos:
db_1 %>% head(n = 5)
db_2 %>% head(n = 4)

## Combinar el conjunto de datos:
db <- bind_cols(db_1,db_2)
db

print("Algo salió mal! la función bind_cols() no tiene en cuenta el identificador de cada observación.") 

### **1.3 Adicionar variables a un conjunto de datos:** `join()`

## Puede adicionar variables a un conjunto de datos usando la familia de funciones de `join()`:
data_1 <- tibble(Casa=c(101,201,201,301),
              Visita=c(2,1,2,1),
              Sexo=c("Mujer","Mujer","Hombre","Hombre"))
data_2 <- tibble(Casa=c(101,101,201,201),
              Visita=c(1,2,1,2),
              Edad=c(23,35,7,24),
              Ingresos=c(500000,1000000,NA,2000000))

#### **Ejemplo: left_join()**
df <- left_join(x=data_1,y=data_2,by=c("Casa","Visita"))

#### **Ejemplo: right_join()**
df <- right_join(x=data_1,y=data_2,by=c("Casa","Visita"))

#### **Ejemplo: inner_join()**
df <- inner_join(x=data_1,y=data_2,by=c("Casa","Visita"))

#### **Ejemplo: full_join()**
df <- full_join(x=data_1,y=data_2,by=c("Casa","Visita"))

#### **Ejemplo: Join sin identificador único**
df <- full_join(x=data_1,y=data_2,by=c("Casa"))

### **1.4 Chequear valores unicos**
df_1 <- tibble(Hogar=1,Visita=1,Sexo=1)
df_2 <- tibble(Hogar=1,Visita=1,Edad=1,Ingresos=1)

## Coincidencia en variables:
intersect(colnames(df_1),colnames(df_2))

## unique
length(unique())
table(duplicated())
nrow(distinct_all())

## **[2.] Aplicación: GEIH**

# **Directorio:** permite dentificar la vivienda.

# **Secuencia_p:** identifica el hogar.

# **Orden:** hace referencia a las personas.
rm(list=ls())

cg <- import("input/Enero - Cabecera - Caracteristicas generales (Personas).csv") %>% clean_names()

ocu <- import("input/Enero - Cabecera - Ocupados.csv") %>% clean_names()

ocu$ocupado <- 1 
  
intersect(colnames(cg) , colnames(ocu))

geih <- left_join(x = cg, y = ocu, by = c("directorio","secuencia_p","orden"))

geih$exp <- gsub(",",".",geih$fex_c_2011.x) %>% as.numeric()
  
sum(geih$exp)

## **[3.] Descriptivas de un conjunto de datos**

# **p6020:** Sexo
# **p6040:** Edad
# **p6960:** ¿Cuántos años lleva afiliado al fondo de pensiones?
# **inglabo:** Ingresos totales.
# **p6450:** ¿Su contrato laboral es verbal o escrito?

### **3.1 Generales**

## descriptivas del ingreso laboral
summary(geih$inglabo)

## descriptivas del ingreso laboral y la edad
geih %>% select(inglabo,p6040) %>% summary()

geih %>% 
select(inglabo,p6040,p6500) %>% 
summarize_all(list(min, max, median, mean), na.rm = T)

### **3.2 Agrupadas**

## ingreso laboral promedio por sexo
geih %>% 
group_by(p6020) %>%  
summarise(promedio_inglabo = mean(inglabo, na.rm = T))

ing_depto <- geih %>% 
             group_by(dpto.x) %>% 
             summarise(promedio_inglabo = mean(inglabo, na.rm = T))

## ingreso laboral promedio por sexo y tipo de contrato
geih %>% 
group_by(p6020,p6450) %>%  
summarise(promedio_inglabo = mean(inglabo, na.rm = T))

ing_depto <- geih %>% 
             group_by(dpto.x,p6020) %>% 
             summarise(promedio_inglabo = mean(inglabo, na.rm = T))

## ingreso laboral promedio/mediano y años promedio en fondo de pension por sexo 
geih %>% 
group_by(p6020) %>%  
summarise(promedio_inglabo = mean(inglabo, na.rm = T),
          mediana_inglabo = median(inglabo, na.rm = T),
          promedio_p6960 = mean(p6960, na.rm = T))

## ingreso laboral promedio/mediano y años promedio en fondo de pension por sexo y tipo contrato
geih %>% 
group_by(p6020,p6450) %>%  
summarise(promedio_inglabo = mean(inglabo, na.rm = T),
        mediana_inglabo = median(inglabo, na.rm = T),
        promedio_p6960 = mean(p6960, na.rm = T))

## guardar resultados en objeto y exportar
descrip <- geih %>% 
           group_by(p6020,p6450) %>%  
           summarise(promedio_inglabo = mean(inglabo, na.rm = T),
                     mediana_inglabo = median(inglabo, na.rm = T),
                     promedio_p6960 = mean(p6960, na.rm = T))
descrip
export(descrip,"output/descriptivas ingreso.xlsx")
